import { SEND_TAGS } from '../actionsConstants';

let initState = new Array();

const tagsReducer = (state = initState, action) => {
    switch(action.type) {
        case SEND_TAGS: 
            console.log("action is ", action);
            return [...action.payload];
        default: 
            return state;
    }
}

export default tagsReducer;