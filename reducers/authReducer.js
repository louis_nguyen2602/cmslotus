import { AUTHENTICATE } from '../actionsConstants';

let initState = {
    isLoggedIn: false,
    step: 'input-mobile-email'
}

const authReducer = (state = initState, action ) => {
    switch(action.type) {
        case AUTHENTICATE: 
            return {...state, ...action.payload};
        default:
            return state;
    }
}

export default authReducer;