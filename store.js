import {
    createStore,
    applyMiddleware,
    combineReducers
  } from 'redux';
  import thunkMiddleware from 'redux-thunk';
  import authReducer from './reducers/authReducer';
  import tagsReducer from './reducers/tagsReducer';
  import pathImageReducer from './reducers/pathImageReducer';

  const rootReducer = combineReducers({
      auth: authReducer,
      tags: tagsReducer,
      pathImage: pathImageReducer

  })

  const initialState = {};

    const store =  createStore(
        rootReducer,
        initialState,
        applyMiddleware(thunkMiddleware)
    )
  

  export default store;