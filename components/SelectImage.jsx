import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { uploadImg } from '../actions/uploadImgAction';

const SelectImage = ({ path, handleChange, useButtonClick, handleClose }) => {
  // const [previewImg, setPreviewImg] = useState(null);
  // const [imgFile, setImgFile] = useState(null);
  // let dispatch = useDispatch();
  // let selectImage = React.createRef();

  // const handleClickAddImg = (event) => {
  //   selectImage.current.click();
  // }

  // const handleChangeFile = (event) => {
  //   let reader = new FileReader();
  //   let file = selectImage.current.files[0];
  //   reader.onloadend = () => {
  //     setPreviewImg(reader.result);
  //   }
  //   if(file) {
  //     setImgFile(file);
  //     reader.readAsDataURL(file);
  //   }
  //   event.target.value = null;

  // }

  // const handleCloseModal = (event) => {
  //   setPreviewImg(null);
  //   handleClose();
  // }

  // const handleUseImg = async (event) => {
  //   let imgUpload = new FormData();
  //   if(imgFile) {
  //     imgUpload.append('image', imgFile);
  //     await dispatch(uploadImg(imgUpload));
  //   }
  // }

    return (
      <div className="modal" tabIndex="-1" role="dialog" id="selectImg">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Choose your picture</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={handleClose}
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <input
                value={path}
                placeholder="your image's path"
                className="form-control"
                onChange={handleChange}
              />
              {/* <img src={previewImg ? previewImg : "/images/add_image_placeholder.svg"} alt="add-img" onClick={handleClickAddImg} className="preview-img" /> */}
              {/* <input type="file" hidden accept="image/*" ref={selectImage} onChange={handleChangeFile} /> */}
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-primary"
                data-dismiss="modal"
                onClick={useButtonClick}
              >
                Use
              </button>
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
                onClick={handleClose}
              >
                Close
              </button>
            </div>
          </div>
        </div>
        <style>{`
                .modal-header {
                    border-bottom: none;
                }
                .modal-footer {
                    border-top: none;
                }
                .preview-img {
                  width: 200px;
                  height: 150px;
                }
            `}</style>
      </div>
    );
  };
  
  export default SelectImage;
  