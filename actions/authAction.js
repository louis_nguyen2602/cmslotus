import { AUTHENTICATE } from '../actionsConstants';

export const nextStepAuth = (isLoggedIn, step) => {
    return {
        type: AUTHENTICATE,
        payload: {
            isLoggedIn,
            step
        }
    }
}

