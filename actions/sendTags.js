import { SEND_TAGS } from '../actionsConstants';

const sendTags = (tags) => {
    return ({
        type: SEND_TAGS,
        payload: tags
    })
}

export default sendTags;