import { API_UPLOAD_IMAGE } from '../APIConfig';
import { uploadNormal2 } from '../helpers/Network';
import { URL_IMAGE } from '../actionsConstants';

export const uploadImgSucess = (res) => {
    return {
        type: URL_IMAGE,
        payload: {
            path: res.data.link
        }
    }
}

export const uploadImg =  (data) => {
    return async dispatch => {
        try {
            let res = await uploadNormal2(API_UPLOAD_IMAGE, data, 'POST');
            dispatch(uploadImgSucess(res));
        } catch(error) {
            console.error("error ", error);
        }
    }
}