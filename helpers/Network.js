import { getCookie,removeCookie } from './cookies.js';
const FETCH_TIMEOUT = 15000;

export default function (url, params = {}, method = 'POST') {
    let token = getCookie('JWT');
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': token || '',
      'Accept-Language': 'vi-VN,vi;q=0.9'
    };
    const opts = {
      mode: 'cors',
      method,
      headers,
    };
    if (method.toLowerCase() !== 'get') {
      opts.body = typeof params === 'object' ? JSON.stringify(params) : params;
    } else if (params) {
      let keys = Object.keys(params);
      if (keys.length > 0) {
        let query = keys.map((key) => {
          return `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`;
        });
        query = query.join('&');
        url += `?${query}`;
      }
    }
    let didTimeOut = false;
  
    //request timeout
    return new Promise(function (resolve, reject) {
      const timeout = setTimeout(function () {
        didTimeOut = true;
        return reject(new Error('Request timed out'));
      }, FETCH_TIMEOUT);
  
      //actual call api
      fetch(url, opts).then(async (response) => {
        try {
          clearTimeout(timeout);
          if (!didTimeOut) {
            if (typeof response === 'object') {
              if (response.status < 200 || response.status >= 300) {
                const contentType = response.headers.get('content-type');
                const res = contentType.indexOf('json') >= 0 &&
                  await response.json();

                if (response.status === 401) {
                  removeCookie();
                  window.location = '/dang-nhap';
                  return;
                }

                if (response.status === 503) {
                  return;
                }
             
                if (response.status >= 500) {
                  return Promise.reject('The server is temporarily unable to service your request due to maintenance downtime or capacity problems. Please try again later.');
                }

                if (response.status === 400) {
                  if (typeof (res) === 'object') {
                    if (res.detail) return Promise.reject(res.detail);
                    let errStr = Object.values(res).toString();
                    return Promise.reject(errStr);
                  }
                  return Promise.reject(res.toString());
                }

                if (response.status === 404) return Promise.reject(404);

                if (res) {
                  return Promise.reject(
                    (typeof res) === 'object' ? JSON.stringify(res) : res);
                }

                return Promise.reject(response.statusText);
              }
              return response.json();
            } else {
              return Promise.reject('Request failed');
            }
          }
        } catch (err) {
          return Promise.reject(err);
        }
      }).then(data => {
        return resolve(data);
      }).catch(err => {
        if (didTimeOut) return;
        return reject(err);
      });
    });
  }

  // interface upload api
  export function uploadNormal(url, data, method = 'POST') {
    
    let token = getCookie('access_token_temp') || getCookie('access_token');

    let didTimeOut = false;
  
    return new Promise(function (resolve, reject) {
      const timeout = setTimeout(function () {
        didTimeOut = true;
        return reject(new Error('Request timed out'));
      }, FETCH_TIMEOUT);
  
      fetch(url, {
        mode: 'cors',
        method,
        headers: {
          'Authorization': token || '',
        },
        body: data,
      }).then(async response => {
        clearTimeout(timeout);
        if (!didTimeOut) {
          if (response.status < 200 || response.status >= 300) {
            if (response.headers.get('content-type').indexOf('json') >= 0) {
              const res = await response.json();
  
              if (response.status >= 500) {
                return Promise.reject('The server is temporarily unable to service your request due to maintenance downtime or capacity problems. Please try again later.');
              }
  
              if (response.status === 400) {
                if (typeof (res) === 'object') {
                  if (res.detail) return Promise.reject(res.detail);
                  let errStr = Object.values(res).toString();
                  return Promise.reject(errStr);
                }
                return Promise.reject(res.toString());
              }
              
              return Promise.reject(res.toString());
            } else {
              throw new Error(response.statusText);
            }
          } else return response.json();
        }
      }).then(data => {
        return resolve(data);
      }).catch(err => {
        if (didTimeOut) return;
        return reject(err);
      });
    });
  }

  export function uploadNormal2(url, data, method = 'POST') {
    
    let didTimeOut = false;
  
    return new Promise(function (resolve, reject) {
      const timeout = setTimeout(function () {
        didTimeOut = true;
        return reject(new Error('Request timed out'));
      }, FETCH_TIMEOUT);
  
      fetch(url, {
        mode: 'cors',
        method,
        headers: {
          'Authorization': 'Client-ID 2015a99b8523a87',
        },
        body: data,
      }).then(async response => {
        clearTimeout(timeout);
        if (!didTimeOut) {
          if (response.status < 200 || response.status >= 300) {
            if (response.headers.get('content-type').indexOf('json') >= 0) {
              const res = await response.json();
  
              if (response.status >= 500) {
                return Promise.reject('The server is temporarily unable to service your request due to maintenance downtime or capacity problems. Please try again later.');
              }
  
              if (response.status === 400) {
                if (typeof (res) === 'object') {
                  if (res.detail) return Promise.reject(res.detail);
                  let errStr = Object.values(res).toString();
                  return Promise.reject(errStr);
                }
                return Promise.reject(res.toString());
              }
              
              return Promise.reject(res.toString());
            } else {
              throw new Error(response.statusText);
            }
          } else return response.json();
        }
      }).then(data => {
        return resolve(data);
      }).catch(err => {
        if (didTimeOut) return;
        return reject(err);
      });
    });
  }