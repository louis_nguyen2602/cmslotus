import App from 'next/app';
import withRedux from 'next-redux-wrapper';
import { Provider } from 'react-redux';
import store from '../store';
import {getCookie} from '../helpers/cookies';

const protectedPaths = ['/'];
const redirectIfLogined = ['/login'];

export function redirectUser(ctx, location) {
    if (ctx.req) {
      ctx.res.writeHead(302, {
        Location: location
      });
      ctx.res.end();
    } else {
      Router.push(location);
    }
}

class myApp extends App {
    static async getInitialProps({Component, ctx}) {
        const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
        const req = ctx.req;
        let token = getCookie('access_token', ctx.req);
        let pathname = ctx.pathname;
        const isProtected = protectedPaths.indexOf(pathname) > -1;
        const isRedirect = redirectIfLogined.indexOf(pathname) > -1;
        if (!!!token && isProtected) {
            redirectUser(ctx, '/login');
          } else if (token && isRedirect) {
            redirectUser(ctx, '/');
        }
        return {
            pageProps
        };
    }

    render() {
        const {Component, pageProps, store } = this.props;
        return <Provider store={store}>
            <Component {...pageProps} />
        </Provider>
    }
}

const makeStore = () => store

export default withRedux(makeStore)(myApp);

