import Head from 'next/head';
import Router from 'next/router';
import { useDispatch } from 'react-redux';
import { nextStepAuth } from '../actions/authAction';
import { removeCookie } from '../helpers/cookies';


export default function Home() {
  let dispatch = useDispatch();
  const handleLogOut = () => {
    removeCookie('access_token');
    dispatch(nextStepAuth(false, 'input-mobile-email'));
    Router.push('/login');
  }

  return (
      <div className="imsd-home-route">
        <div className="header-fixed">
          <div className="imsd-popup__header">
            <div className="logo-viva">
              <img src="/images/lotus-logo.png" alt="logo-lotus" />
              <span className="logo-viva-text">Lotus</span>
            </div>
            <div className="right-header current-user-nav">
              <div className="flex-container flex-rows flex-align-center mt-10">
                <div className="user-info">
                  <div className="user" onClick={handleLogOut}>Logout</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <style jsx>{` 
        .imsd-home-route {
            height: 100%;
            display: flex;
            width: 100%;
            flex-direction: column;
        }
        .imsd-home-route .header-fixed {
          z-index: 1;
        }
        .imsd-home-route .imsd-popup__header {
          height: 48px;
          text-align: center;
          align-items: center;
          background: linear-gradient(0deg,#002d64,#002d64),#fff;
          box-shadow: 0 1px 3px rgba(0,0,0,.08);
          border: none;
          justify-content: center;
          display: block;
        }
        .imsd-home-route .imsd-popup__header .logo-viva {
          float: left;
          margin: 4px 0 0 15px;
          display: flex;
          color: #fff;
          align-items: center;
          justify-content: center;
        }
        .logo-viva-text {
          margin-left: 10px;
        }
        .imsd-home-route .current-user-nav {
          float: right;
          padding-top: 3px;
          text-align: left;
          color: #fff;
          margin-right: 15px;
          display: flex;
          font-size: 13px;
          font-family: SF Display;
        }
        .current-user-nav {
          position: relative;
          cursor: pointer;
        }
        .imsd-home-route .mt-10 {
          margin-top: 6px;
        }
        .flex-container.flex-rows {
          flex-direction: row;
        }
        .flex-align-center {
          align-items: center;
        }
        .flex-container {
          display: flex;
          width: 100%;
          height: 100%;
          flex-grow: 1;
          overflow: hidden;
        }
        .user {
          font-size: 14px;
          line-height: 2;
          margin-top: 4px;
        }
       
      `}</style>
      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }
      
        * {
          box-sizing: border-box;
        }
      `}</style>
      </div>
      
  )
}
