import Head from 'next/head';
import { useSelector, useDispatch } from 'react-redux';
import renderTags from '../helpers/renderTags';

const Preview = () => {
    const tags = useSelector((state) => state.tags);
    console.log("preview component tags is ", tags);
    return (
        <div>
            <Head>
                <title>Piki</title>
                <link rel='shortcut icon' type='image/x-icon' href='/images/lotus-logo.png' />
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossOrigin="anonymous" />
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
            </Head>
            <div className="container">
                {renderTags(tags)}
            </div>
            <style>{`
                .block-default {
                    width: 50%;
                    height: 150px;
                    border: 1px dashed blue;
                    background-color: #fff;
                    position: absolute;
                    overflow: hidden;
                    top: 20px;
                    left: 20px;
                }
                .button-default {
                    display: inline-block;
                    margin-bottom: 0;
                    font-weight: 400;
                    text-align: center;
                    white-space: nowrap;
                    vertical-align: middle;
                    -ms-touch-action: manipulation;
                    touch-action: manipulation;
                    cursor: pointer;
                    background-image: none;
                    border: 1px solid transparent;
                    padding: 6px 12px;
                    font-size: 14px;
                    line-height: 1.42857143;
                    border-radius: 4px;
                    -webkit-user-select: none;
                    -moz-user-select: none;
                    -ms-user-select: none;
                    user-select: none;
                    color: #fff;
                    background-color: #5cb85c;
                    position: absolute;
                    top: 0px;
                    left: 0px;
                }
                .para-default {
                    background-color: transparent;
                    position: absolute;
                }
                .section-default {
                    width: 100%;
                    height: 300px;
                    position: relative;
                    border: 1px dashed red;
                    background-color: #fff;
                }
                .img-default {
                    background-repeat: no-repeat;
                    background-position: left top;
                    background-size: cover;
                    background-attachment: scroll;
                    background-origin: content-box;
                    position: absolute;
                    margin: 0 auto;
                    width: 200px;
                    height: 150px;
                }
            `}</style>
        </div>
    )
}

export default Preview;
