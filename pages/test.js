import {useState, useEffect} from 'react';

const Test = () => {
  const [x, setX] = useState(0);
  const [y, setY] = useState(0);

  const handleClick = (event) => {
    setX(x+1);
  }

  // useEffect(() => {
  //   console.log("effect")
  //   setY(x*x);
  //   return function A() {
  //     console.log("cleaning");
  //     console.log('x is ', x);
  //   }
  // },[x])
  const handleMouseDown = (event) => {
    console.log("mouse down on ", event.target.id);
    return;
  }

  const handleMouseUp = (event) => {
    console.log("mouse up on ", event.target.id);
    return;
  }

  const handleBlur = (event) => {
    console.log("blur on", event.target.id);
  }

  console.log("render");
  return (
    <div>
     <p contentEditable={true} id="para" onBlur={handleBlur} onMouseUp={handleMouseUp} onMouseDown={handleMouseDown} suppressContentEditableWarning={true} tabIndex={1}>hello</p>
     <p>dhfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff</p>
    </div>
    
  )


}

export default Test;